[![pipeline status](https://gitlab.com/osama-shawir/My_Zola_Website/badges/main/pipeline.svg)](https://gitlab.com/osama-shawir/My_Zola_Website/-/commits/main)

# Continuous Delivery of Rust Microservice

This microservice is my personal portfolio website built using Zola, a fast static site generator in a single binary with everything built-in that is Rust based. The website is automatically deployed to GitLab Pages at [osama-shawir.gitlab.io](https://osama-shawir.gitlab.io/My_Zola_Website/) upon each push to the main branch, thanks to the integrated CI/CD pipeline. The video demo of this website can be viewed [here](https://youtu.be/k2FXRbfIRXo)

[![YouTube Video](https://img.youtube.com/vi/k2FXRbfIRXo/0.jpg)](https://youtu.be/k2FXRbfIRXo)

## Getting Started

This project is already set up for you to get started. If you're new to Zola, you might find the following resources helpful:

- [Zola Documentation](https://www.getzola.org/documentation/getting-started/installation/)

## Project Structure

This project follows the standard Zola project structure:

- `content`: This directory contains the Markdown files for your pages and sections.
- `static`: This directory contains static files like images, CSS, and JavaScript.
- `templates`: This directory contains your HTML templates using the Tera templating engine.
- `config.toml`: This file is the main configuration file for your Zola site.
- `.gitlab-ci.yml`: This file contains the configuration for the CI/CD pipeline that automatically builds and deploys the website to GitLab Pages.
- `Dockerfile`: This file contains the configuration for the docker container on which the website is built and deployed to gitlab pages on every push.

## Microservice

The website is built as a microservice in Rust, providing a web service. The microservice is containerized using Docker, as defined in the Dockerfile.

## Running Locally

To run this project locally, navigate to the project directory and run the following command:

```bash
zola serve
```

This will start a local server and you can view your site at http://localhost:1111.

## Deployment

This project is set up to be deployed using GitLab Pages. The CI/CD pipeline defined in the `.gitlab-ci.yml` file automatically builds the website and deploys it to GitLab Pages upon each push to the main branch. This ensures that the live website is always up-to-date with the latest changes.

## Docker Configuration

The Dockerfile in this project sets up the environment for running the Rust microservice. It ensures that the service is properly containerized and ready for deployment. The Docker configuration contributes to the CI/CD pipeline by building and deploying the Docker container to GitLab Pages upon each push to the main branch.